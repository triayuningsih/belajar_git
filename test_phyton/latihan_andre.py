def cetak_bintang(jumlah_baris):
    for baris in range(1, jumlah_baris + 1):
        for kolom in range(1, baris + 1):
            print("*", end="")
        print()

# Meminta input jumlah baris dari pengguna
jumlah_baris = int(input("Masukkan jumlah baris: "))

# Memanggil fungsi cetak_bintang dengan jumlah baris yang diinputkan oleh pengguna
cetak_bintang(jumlah_baris)
